// console.log('Hello');

/*function antiLoop(){
let counter = 1000
while (counter > 0){
	console.log(counter);
	counter = counter - 1;
}

} 
*/




/* Selection Control Structure
	-sorts out wether the statement/s are to be executed based on the condition whether it is true or false
	
	if .. else statement

	Syntax:
		if( condition ) {
		//statment
		} else { 
			//statement
		}

		if statement - executes a statement if a specified condition is true

		Syntax:
			if(condition) {
				//statement
			}

*/

let numA = -1;

if (numA > 0) {
	console.log('Hello');
}

console.log(numA>0);

let city = "New York";

if(city === "New York") {
	console.log("Welcome to New York City");
}

/*
	Else if
		- executes a statement if previous conditions are false and if the specified condition is true
		- the "else if" clause is optional and can be added to capture additional conditions to change the flow of a program

*/

let numB = 1;

if (numA > 0) {
	console.log("Hello");
} else if (numB > 0) {
	console.log("World");
}

// Another Example:
city = "Tokyo";

if(city === "New York") {
	console.log("Welcome to New York City");
} else if (city === "Tokyo") {
	console.log("Welcome to Tokyo!");
}

/*
 	else statement
		-executes a statement if all other conditions are false
*/

if(numA > 0) {
	console.log("Hello");
} else if (numB === 0) {
	console.log("World");
} else {
	console.log ("Again");
}

// let age = parseInt(prompt("Enter your age: "));

/*if(age <= 18) {
	console.log("Not allowed to drink!");
} else {
	console.log("Matanda kaba, shot na!");
}*/

/*
	Mini Activity:
		Create a conditional statement that if height is below 150, display 
*/

/*console.clear();

let height = parseInt(prompt("Enter your height: "));

function checkHeight(height){
	if (height < 150) {
		console.log("Did not passed the minimum height requirement")

	} else {
		console.log("Passed the minimum height requirement")
	}
}

checkHeight(height);
*/

console.clear();
function determineTyphoonIntensity(windspeed){

	if(windspeed < 30) {
		return 'Not a typhoon yet'

	} else if (windspeed <= 61) {
		return 'Tropical Depresion detected'
	} else if (windspeed >= 62 && windspeed <= 88){
		return 'Tropical storm detected.'
	} else if (windspeed >= 89 && windspeed <= 117) {
		return 'Severe tropical storm detected'
	} else {
		return 'Typhoon detected.'
	}
}

// console.log(determineTyphoonIntensity(70));
// invoke function with argument
/*
message = determineTyphoonIntensity(70);
console.log(message);
console.warn(message);

if (message == 'Tropical storm detected.'){
	console.warn(message);
}
*/
// Truthy and Falsy 
/*
	in JS. a truthy value is a value that is considered true when encountered in a boolean context.

	Falsy Values
		1. false
		2. 0
		3. -0
		4. ""
		5. null
		6. undefined
		7. 

*/

if (true) {
	console.log("Truthy");
}

if (1) {
	console.log("Truthy");
}

if ([]) {
	console.log("Truthy");
}

if (false) {
	console.log("Falsy");
} 

if (0) {
	console.log("Falsy");
}

if (undefined) {
	console.log("Falsy");
}

// Conditional (Ternary) Operator
/*
	Ternary Operator takes in three operand
		1. condition
		2. expression to execute if the condition is true or truthy
		3. expression to execute if the condition is false or falsy

		Syntax:
			(condition) ? ifTrue_expression : ifFalse_expression
*/

// single statement execution
/*let ternaryResult = (1 < 18) ? "Condition is true" : "Condition is False"
console.log("result of the ternary operator: " + ternaryResult);
*/

// Multiple statement Execution

/*let name; 

function isOfLegalAge(){
	name = 'John'
	return 'You are of the legal age limit';
}

function isUnderAge() {
	name = 'Jane';
	return 'You are under the age limit';
}

let age = parseInt(prompt('what is your age?'));
console.log(age);
let legalAge = (age > 18) ? isOfLegalAge() : isUnderAge();
console.log("Resuly of Ternary Operator in Functions: " + legalAge +', '+ name);
*/

// Switch Statement

/*
 can be used as an alternative to an if .. statement where the data to be used in the condition is of an expected input.

 syntax:
 	Switch (expression) {
		case <value>:
			statement to execute;
			break;
		
		default:
			statement;
			break;
 	}
*/

/*let day = prompt("what day of the week is it today?").toLowerCase();

console.log(day);

switch (day) {

	case 'monday':
		console.log("The color of the day is red.");
		break;
	case 'tuesday':
		console.log("The color of the day is orange.");
		break;
	case 'wednesday':
		console.log("The color of the day is yellow.");
		break;
	case 'thursday':
		console.log("The color of the day is green.");
		break;
	case 'friday':
		console.log("The color of the day is blue.");
		break;
	case 'saturday':
		console.log("The color of the day is indigo.");
		break;
	case 'sunday':
		console.log("The color of the day is violet.");
		break;
	default:
		console.log("Please input a valid day.");
		break;
}*/

// Try-Catch-Finally Statement

/*
	- try catch are commonly used for error handling

*/

function showIntensityAlert(windSpeed){
	
	try {
		alerat(determineTyphoonIntensity(windSpeed))
	}

	
	catch (error) {
		console.log(typeof error)
		console.log(error)
		console.warn(error.message)
	}
	

	finally {
		alert("Intensity updates will show new alert!!")
	}
}

showIntensityAlert(56);